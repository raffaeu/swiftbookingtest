﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftBookingTest.Web.Extras
{
    /// <summary>
    /// Contains magic strings used acrossed the project
    /// </summary>
    public class Constants
    {
        public static string PAGE_TITLE = "GetSwift | Delivery Management Software";
    }
}