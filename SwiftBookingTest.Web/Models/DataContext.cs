﻿/****************************** Module Header ******************************\
Last Edit: Raffaele Garofalo
\***************************************************************************/

using System.Data.Entity;

namespace SwiftBookingTest.Web.Models
{
    public class DataContext : DbContext
    {
        public DataContext():base("DatabaseConnection")
        {
            Database.SetInitializer<DataContext>(new CreateDatabaseIfNotExists<DataContext>());
        }

        #region Public Properties

        public DbSet<Customer> Customers { get; set; }

        #endregion Public Properties
    }
}