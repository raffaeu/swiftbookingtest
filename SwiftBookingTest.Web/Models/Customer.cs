﻿/****************************** Module Header ******************************\
Module Name:  <File Name>
Project:      <Sample Name>
Copyright (c) Mproof B.V.

Last Edit: Raffaele Garofalo
\***************************************************************************/
/****************************** Module Header ******************************\
Last Edit: Raffaele Garofalo
\***************************************************************************/

using System;

namespace SwiftBookingTest.Web.Models
{
    /// <summary>
    /// It represents a Customer 
    /// </summary>
    public class Customer
    {
        #region Public Constructors

        /// <summary>
        /// Create a new Customer and assign an ID 
        /// </summary>
        public Customer()
        {
            this.Id = Guid.NewGuid();
        }

        #endregion Public Constructors

        #region Public Properties

        /// <summary>
        /// The address used for the delivery 
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// The Full name of the Customer 
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// The Unique Id of the Customer 
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// The Telephone Number 
        /// </summary>
        public string Phone { get; set; }

        #endregion Public Properties
    }
}