﻿/**
 * Define the main entry
 */
var getSwiftApp = angular.module('getSwiftApp', []);

/*
 * Main controller
 */
getSwiftApp.controller('MainController', ['$scope', '$http', function ($scope, $http) {

    // hold the getSwiftLog
    $scope.getSwiftLog = '';

    // list of customers
    $scope.customers = [];

    // current customer
    $scope.customer = {
        fullName: '',
        address: '',
        phone: '',
        isValid: function () {
            if (this.address === '' || this.fullName === '' || this.phone === '') {
                return false;
            }

            return true;
        },
        init: function () {
            this.fullName = '';
            this.address = '';
            this.phone = '';
        },
        dto: function () {
            return {
                FullName: this.fullName, Phone: this.phone, Address: this.address
            };
        }
    };

    // drop off addresses
    $scope.dropOffList = [
        '123 Some address st, Melbourne, Australia',
        '123 Another address st, New York',
        '668 Joel St, Brisbane, Australia'
    ];

    // validate and submit customer
    $scope.submitCustomer = function () {
        if ($scope.customer.isValid()) {
            $http({
                method: 'POST',
                url: '/api/Delivery',
                data: $scope.customer.dto()
            }).then(function successCallback(response) {
                $scope.customer.init();
                $scope.loadCustomers();
            }, function errorCallback(response) {
                Materialize.toast('Server error', 4000);
            });
        } else {
            Materialize.toast('Please check your input, data is invalid', 4000);
        }
    };

    // load customers
    $scope.loadCustomers = function () {
        $http({
            method: 'GET',
            url: '/api/Delivery'
        }).then(function successCallback(response) {
            // load result
            $scope.customers = response.data;
        }, function errorCallback(response) {
            Materialize.toast('Server error', 4000);
        }); $scope.customer.init();
    };

    // delete customer
    $scope.deleteCustomer = function (id) {
        $http({
            method: 'DELETE',
            url: '/api/Delivery/' + id
        }).then(function successCallback(response) {
            $scope.loadCustomers();
        }, function errorCallback(response) {
            Materialize.toast('Server error', 4000);
        });
    };

    // send the request
    $scope.sendToGetSwift = function (id, location) {
        // verify customer exists
        var selectedCustomer = $.grep($scope.customers, function (e) { return e.Id === id; });
        if (!selectedCustomer || selectedCustomer.length < 1) {
            Materialize.toast('Customer not found', 4000);
            return;
        }

        if (!location || location === '') {
            Materialize.toast('Choose a drop off', 4000);
            return;
        }

        selectedCustomer = selectedCustomer[0];
        // send request
        $http({
            method: 'POST',
            url: ' https://app.getswift.co/api/v2/deliveries',
            data: {
                "apiKey": "3285db46-93d9-4c10-a708-c2795ae7872d",
                "booking": {
                    "pickupDetail": {
                        "name": selectedCustomer.FullName,
                        "phone": selectedCustomer.Phone,
                        "address": selectedCustomer.Address
                    },
                    "dropoffDetail": {
                        "name": selectedCustomer.FullName,
                        "phone": selectedCustomer.Phone,
                        "address": location
                    }
                }
            }
        }).then(function successCallback(response) {
            $scope.customer.init();
            $scope.getSwiftLog = response.data;
            Materialize.toast('Request processed', 4000);
            window.location.hash = "#" + 'logAnchor';
        }, function errorCallback(response) {
            $scope.getSwiftLog = response.data;
            Materialize.toast('Server error', 4000);
            window.location.hash = "#" + 'logAnchor';
        });
    };
}]);