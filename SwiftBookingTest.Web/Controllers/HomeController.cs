﻿/****************************** Module Header ******************************\
Last Edit: Raffaele Garofalo
\***************************************************************************/

using System.Web.Mvc;

namespace SwiftBookingTest.Web.Controllers
{
    public class HomeController : Controller
    {
        #region Public Methods

        public ActionResult Index()
        {
            return View();
        }

        #endregion Public Methods
    }
}