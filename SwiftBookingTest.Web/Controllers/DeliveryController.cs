﻿/****************************** Module Header ******************************\
Last Edit: Raffaele Garofalo
\***************************************************************************/

using SwiftBookingTest.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SwiftBookingTest.Web.Controllers
{
    public class DeliveryController : ApiController
    {
        public DeliveryController()
        {

        }

        #region Public Methods

        [HttpPost]
        public HttpResponseMessage Post(Customer customer)
        {
            using (DataContext context = new DataContext())
            {
                context.Customers.Add(customer);
                context.SaveChanges();
            }


            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        [HttpGet]
        public IEnumerable<Customer> Get()
        {
            using (DataContext context = new DataContext())
            {
                return context.Customers.ToList();
            }
        }

        [HttpDelete]
        public HttpResponseMessage Delete(Guid id)
        {
            using (DataContext context = new DataContext())
            {
                var customer = context.Customers.FirstOrDefault(x => x.Id == id);
                context.Customers.Remove(customer);
                context.SaveChanges();
            }

            return new HttpResponseMessage(HttpStatusCode.OK);

        }
        #endregion Public Methods
    }
}